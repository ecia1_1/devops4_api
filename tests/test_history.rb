ENV['APP_ENV'] = 'test'
ENV['TK'] = File.open('token').read

require './src/main'
require 'test/unit'
require 'rack/test'

class HistoryTest < Test::Unit::TestCase
    include Rack::Test::Methods

    def app
        Sinatra::Application
    end

    def test01_userhistory
        header 'Authorization', ENV['TK']
        get '/api/v1/history'
        assert_equal 200 , last_response.status
    end

    def test02_usernotauth
        get '/api/v1/history'
        assert_equal 403 , last_response.status
    end
end