ENV['APP_ENV'] = 'test'

require './src/main'
require 'test/unit'
require 'rack/test'

class AutocompleteTest < Test::Unit::TestCase
    include Rack::Test::Methods

    def app
        Sinatra::Application
    end

    def test01_query
        get '/api/v1/autocomplete/poli'
        assert_equal last_response.status, 200
    end
end