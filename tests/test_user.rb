ENV['APP_ENV'] = 'test'

require './src/main'
require 'test/unit'
require 'rack/test'

class UserTest < Test::Unit::TestCase
    include Rack::Test::Methods

    def app
        Sinatra::Application
    end

    def test00_init
        get '/test'
        assert_equal 404,last_response.status
    end

    def test01_if_login_empty
        post '/api/v1/login'
        assert_equal last_response.status, 400
    end

    def test02_if_login_failed
        post '/api/v1/login', {'username':'nobody', 'password':'nobody'}.to_json
        assert_equal last_response.status, 401
    end

    def test03_if_user_created
        #TODO:POST CREATE USER
        user1 = User.create!(name:'Admin ADMIN',username:'admin-test',password:'admin',role:'ADMINISTRATEUR')
        user2 = User.where(username:'admin-test').first
        assert_equal user1, user2
    end

    def test04_if_login_succeed
        post '/api/v1/login', {'username':'admin-test', 'password':'admin'}.to_json
        res = JSON.parse(last_response.body).symbolize_keys
        File.write('token', res[:token])
        assert_equal last_response.status, 200
    end
end