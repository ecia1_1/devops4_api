ENV['APP_ENV'] = 'test'
ENV['TK'] = File.open('token').read

require './src/main'
require 'test/unit'
require 'rack/test'

class SearchTest < Test::Unit::TestCase
    include Rack::Test::Methods

    def app
        Sinatra::Application
    end

    def test01_fullquery
        header 'Authorization', ENV['TK']
        post '/api/v1/search', {'term':'poli', 'dateStart':'1500-02-25T00:00:00Z', 'dateEnd':'2000-02-25T00:00:00Z' }.to_json
        assert_equal 200 , last_response.status
        assert last_response.body.include?('results')
        assert last_response.body.include?('facets')
        assert last_response.body.include?('history')
    end

    def test02_partialquery
        header 'Authorization', ENV['TK']
        post '/api/v1/search', {'term':'poli'}.to_json
        assert_equal 200 , last_response.status
        assert last_response.body.include?('results')
        assert last_response.body.include?('facets')
        assert last_response.body.include?('history')
    end

    def test03_badquery
        header 'Authorization', ENV['TK']
        post '/api/v1/search', {'aaa':'poli'}.to_json
        assert_equal 400 , last_response.status
    end

    def test03_usernotauth
        post '/api/v1/search', {'term':'poli'}.to_json
        assert_equal 403 , last_response.status
    end

    def test04_textquery
        get '/api/v1/search/text/12148-bpt6k573083d'
        assert_equal 200 , last_response.status
    end

    def test05_badtextquery
        get '/api/v1/search/text/12148-bpt6k573083ef'
        assert_equal 404 , last_response.status
    end
end