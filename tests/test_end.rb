ENV['APP_ENV'] = 'test'

require './src/main'
require 'test/unit'
require 'rack/test'

class EndTest < Test::Unit::TestCase
    include Rack::Test::Methods

    def app
        Sinatra::Application
    end

    def test06_if_user_deleted
        User.where(username:'admin-test').delete
        user2 = User.where(username:'admin-test').first
        assert_equal user2, nil
    end
end