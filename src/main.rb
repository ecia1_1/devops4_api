require 'sinatra'
require 'openssl'
require 'jwt'
require 'mongoid'
require 'time_difference'
require "sinatra/namespace"
require 'sinatra/cors'
require 'mongo'
require 'rest-client'

set :bind, '0.0.0.0'
set :allow_origin, "http://localhost:9998 http://localhost:9999 http://localhost:9997 http://localhost http://localhost:9995 http://localhost:9996 http://eci04.univ-lr.fr:443 http://eci04.univ-lr.fr:80 http://eci04.univ-lr.fr"
set :allow_methods, "GET,HEAD,POST,PUT,PATCH,DELETE,OPTIONS"
set :allow_headers, "content-type,if-modified-since,cache-control,Postman-Token,Authorization"
set :expose_headers, "location,link"
set :allow_credentials, true

Mongoid.load! "mongoid.config"
Mongoid::QueryCache.enabled = true
# Models
Dir[File.join(__dir__, 'models', '*.rb')].each(&method(:require))

# Création du compte administrateur et utilisateur par défaut
User.create!(name:'Paul',username:'admin',password:'admin',role:'ADMINISTRATEUR')
User.create!(name:'Jean-Michel',username:'user',password:'user',role:'USER')

# Insertion des données nécessaire au bon fonctionnement
#require_relative 'autorun'

# Authentification
require_relative 'load_pki'
require_relative 'autorize'
require_relative 'v1/login'

# API
Dir[File.join(__dir__, 'v1', '*.rb')].each(&method(:require))