class User
  include Mongoid::Document

  field :name, type:String

  field :username, type:String
  field :password, type:String
  field :role, type:String

  validates :name, presence:true
  validates :role, presence:true
  validates :username, presence:true

  index({ name:1 }, { name: "user_name_idx" })
  index({ username:1 }, { name: "user_username_idx", unique:true })
  index({ password:1 }, { name: "user_pwd_idx" })

end

class UserSerializer
  def initialize(user)
    @user = user
  end

  def as_json(*)
    {
        id:@user.id.to_s,
        name:@user.name,
        role:@user.role,
        mail:@user.mail,
    }
  end
end