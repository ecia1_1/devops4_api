class History
    include Mongoid::Document
    include Mongoid::Timestamps

    field :text, type:String
    field :dateStart, type:String
    field :dateEnd, type:String
    field :user, type:String

    validates :text, presence:true
    validates :dateStart, presence:true
    validates :dateEnd, presence:true
end

class HistorySerializer
    def initialize(history)
        @history = history
    end

    def as_json(*)
        {
            id:@history.id.to_s,
            titre:@history.text,
            dateStart:@history.dateStart,
            dateEnd:@history.dateEnd,
        }
    end
end
