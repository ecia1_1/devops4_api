post '/api/v1/login' do
  body = json_params

  ( (body.key? :username) && (body.key? :password) && body[:password] != "" ) ? user = User.where(username:body[:username], password:body[:password]).first : user = nil

  if user != nil
      resume = {username: user.username.to_s,
                role: user.role,
                name: user.name,
                id: user.id.to_s,
                exp: Time.now.to_i + 43200
              }
      @token = JWT.encode(resume, $signing_key, "RS256", headers)

    halt 200, {role: user.role,
               name: user.name,
               id: user.id.to_s,
               token:@token
            }.to_json
  else
      @token = nil
      halt 401
  end
end