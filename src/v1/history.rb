get '/api/v1/history' do
    results = auth_user?
    if results
        History.where(user:results['id']).order_by(created_at: :desc).limit(30).map { |history| HistorySerializer.new(history) }.to_json
    else
        halt 401
    end
end