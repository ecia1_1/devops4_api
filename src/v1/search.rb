before do
    content_type :json
end

get '/api/v1/search/text/:id' do
    response = RestClient.get("http://eci:eci_pass@americana.univ-lr.fr/solr/devops/select?q=*&fl=id&fl=full_text_tfr_siv&fq=id:"+params[:id],headers={})
    if response.code == 200
        res = JSON.parse(response)
        if res['response']['numFound'] == 1
            (res['response']['docs'][0]['full_text_tfr_siv']).to_json
        else
            halt 404
        end
    else
        halt 400
    end
end

post '/api/v1/search' do
    body = json_params
    unless body.has_key? :term
        halt 400
    end

    #results = auth_user?
   # if results
    #    history = History.create(text:body[:term],dateStart:(body.has_key? :dateStart) ? body[:dateStart] : '*',dateEnd:(body.has_key? :dateEnd) ? body[:dateEnd] : '*',user:results['id'])

        # Par défaut, trié par pertinence
        url = "http://eci:eci_pass@americana.univ-lr.fr/solr/devops/select?rows=40"+
            "&hl=on"+
            "&hl.fl=full_text_tfr_siv"+
            "&hl.method=unified"+
            "&fl=id&fl=title_ssi&fl=date_dtsi"+
            "&hl.fragsize=100"+
            "&hl.encoder=html"+
            "&facet.field=title_ssi&q="+body[:term]

        response = RestClient.get(url,headers={})
        if response.code == 200
            parsed_req = JSON.parse(response)
            res = {}
            res["results"] = []
            parsed_req['response']['docs'].each.with_index { |doc, index|
                res["results"].push(doc)
                if parsed_req['highlighting'][doc['id']].length > 0 && parsed_req['highlighting'][doc['id']]['full_text_tfr_siv'][0] != nil
                    res["results"][index]['full_text_tfr_siv'] = parsed_req['highlighting'][doc['id']]['full_text_tfr_siv'][0].truncate(65)
                else
                    res["results"][index]['full_text_tfr_siv'] = "Pas d'extrait"
                end

            }
            res["facets"] = {}
            facets = parsed_req['facet_counts']['facet_fields']['title_ssi']
            facets.each.with_index { |facet, index| index % 2 == 0 ? res["facets"][facet] = facets[index+1] : nil }
            #res["history"] = HistorySerializer.new(history).to_json
            res.to_json
        else
            halt 400
        end
    #else
    #    halt 401
    #end


end