get '/api/v1/autocomplete/:search' do
    url = "http://eci:eci_pass@americana.univ-lr.fr/solr/devops/suggest?q="+params[:search]
    response = RestClient.get(url,headers={})
    if response.code == 200
        suggests = JSON.parse(response)['suggest']['mySuggester'][params[:search]]['suggestions']
        res = []
        suggests.each {|suggest|
            res.push(suggest['term'])
        }
        res.to_json
    else
        halt 400
    end
end
