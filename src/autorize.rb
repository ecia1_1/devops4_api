helpers do
  # helper to extract the token from the session, header or request param
  # if we are building an api, we would obviously want to handle header or request param
  def extract_token
    # check for the access_token header
    unless env["HTTP_AUTHORIZATION"]
      halt 403
    end

    token = env["HTTP_AUTHORIZATION"].split(' ').last

    if token
      return token
    end

    return nil
  end

  # check the token to make sure it is valid with our public key
  def authorized!
    @token = extract_token
    begin
      payload, headers = JWT.decode(@token, $verify_key, true, { algorithm: 'RS256' })

      @exp = payload["exp"]

      # check to see if the exp is set (we don't accept forever tokens)
      if @exp.nil?
        halt 400 # Le token doit expirer
      end

      @exp = Time.at(@exp.to_i)

      # make sure the token hasn't expired
      if Time.now > @exp
        halt 401 # Token expiré
      end

      @user_id = payload["user_id"]

      return payload

    rescue JWT::DecodeError => e
      halt 401
    end
  end

  def authorized?
    @token = extract_token
    begin
      payload, headers = JWT.decode(@token, $verify_key, true, { algorithm: 'RS256' })
      return payload

    rescue JWT::DecodeError => e
      return {"role":"INVALID"}
    end
  end

  # Regarde si l'utilisateur a les droits : LICENCIE ou ENTRAINEUR ou ADMINISTRATEUR
  def auth_user!
    results = authorized!
    if results["role"] == "USER" || results["role"] == "ADMINISTRATEUR"
      return true
    end
    halt 403
  end

  def auth_user?
    results = authorized?
    if results["role"] == "USER" || results["role"] == "ADMINISTRATEUR"
      return results
    end
    return false
  end

  # Regarde si l'utilisateur a les droits : ADMINISTRATEUR
  def auth_admin!
    if authorized!["role"] == "ADMINISTRATEUR"
      return true
    end
    halt 403
  end

  def auth_admin?
    if authorized?["role"] == "ADMINISTRATEUR"
      return true
    end
    return false
  end

  def base_url
    @base_url ||= "#{request.env['rack.url_scheme']}://{request.env['HTTP_HOST']}"
  end

  def json_params
    begin
      JSON.parse(request.body.read).symbolize_keys
    rescue JSON::ParserError
      halt 400, { message:'Invalid JSON : '+request.body.read }.to_json
    end
  end
end