######
# first we load up the private and public keys that we will use to sign and verify our JWT token
# using RS256 algo
######

signing_key_path = File.expand_path("../../app.rsa", __FILE__)
verify_key_path = File.expand_path("../../app.rsa.pub", __FILE__)

$signing_key = ""
$verify_key = ""

File.open(signing_key_path) do |file|
  $signing_key = OpenSSL::PKey.read(file)
end

File.open(verify_key_path) do |file|
  $verify_key = OpenSSL::PKey.read(file)
end

# enable sessions which will be our default for storing the token
enable :sessions

#this is to encrypt the session, but not really necessary just for token because we aren't putting any sensitive info in there
set :session_secret, 'FSQ098AZ9T0809R83207589RN9RNV973Z987NRZE98TG7E9NZEV0789EZ7R098FEZ7NG0987NR98Z3E7RN839570RTE98N7ZE98RVN789E07NV98SE'