FROM aprimault/sinatra-alpine:latest

MAINTAINER PRIMAULT Alexandre <alexandre.primault@hotmail.com>

USER root

# disable interactive functions
ENV DEBIAN_FRONTEND noninteractive

ADD ./ /app/api

####### Fin

ENV PORT 4000
EXPOSE 4000

VOLUME /data/db
WORKDIR /app/api

CMD ["ruby", "./src/main.rb"]